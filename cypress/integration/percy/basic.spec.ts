/// <reference types="cypress" />
/// <reference types="@percy/cypress" />

context("Percy Demo", () => {
  before(() => {
    cy.visit("http://localhost:8080/");
  });

  it("should have heading and button", () => {
    // Take a snapshot for visual diffing
    cy.percySnapshot();
    cy.get("h1").contains("This is a heading");
    cy.get("button").contains("Show Text");
  });

  it("should toggle text on clicking button", () => {
    cy.get("button").click().contains("Hide Text");
    cy.get(".toggle-body").contains(
      "This is some random text which toggles on button click."
    );
    // Take a snapshot for visual diffing
    cy.percySnapshot();
    cy.get("button").click().contains("Show Text");
    cy.get(".toggle-body").should("not.exist");
  });
});
